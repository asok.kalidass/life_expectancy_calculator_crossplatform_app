﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LifeExpectancy_.Models
{
    public class Country
    {
        public string Name { get; set; }
        public string CountryCode { get; set; }
    }
}
