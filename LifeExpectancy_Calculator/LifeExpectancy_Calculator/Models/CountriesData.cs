﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LifeExpectancy_.Models
{
    public class CountriesData
    {
        public static IList<Country> Countries { get; private set; }

        static CountriesData()
        {
            Countries = new List<Country>();

            Countries.Add(new Country
            {
                Name = "India",
                CountryCode = "IND",
            });

            Countries.Add(new Country
            {
                Name = "Argentina",
                CountryCode = "ARG"
            });

            Countries.Add(new Country
            {
                Name = "China",
                CountryCode = "CHN"
            });


        }
    }
}
