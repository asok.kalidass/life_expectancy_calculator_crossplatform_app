﻿using LifeExpectancy_.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using LifeExpectancy_.Manager;
using LifeExpectancy_.Models;

namespace LifeExpectancy_
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            populateCountryList();            
            BindingContext = new LifeExpectancyPageViewModel();
        }

        /// <summary>
        /// On country ddl select event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnCountryClicked(object sender, EventArgs e)
        {
            var country = (Country)ddlCountry.SelectedItem;
            CalculateLifeExpectancyByRegion(country.CountryCode);
        }

        /// <summary>
        /// calculate the life expectancy 
        /// </summary>
        /// <param name="countryCode"></param>
        void CalculateLifeExpectancyByRegion(string countryCode)
        {
            var elapsedDays = (DateTime.Today - dob.Date).TotalDays * 0.0027379070; //one day = 0.0027379070 years
            //get the life expecatancy of the chosen country from whoz data.
            var lifeExpectancy = Task.Run(async () => await new LifeExpectancyManager().GetLifeExpectancyByCountryAsync(countryCode));
            //wait until the data is fetched
            lifeExpectancy.Wait();
            //remaining years
            var avgYearsRemaing = Convert.ToDouble(lifeExpectancy.Result) - elapsedDays;
            //remaining months
            var avgMonthsRemaining = avgYearsRemaing - Math.Truncate(avgYearsRemaing);
            var remainingMonths = avgMonthsRemaining * 12;
            //remaining days
            var avgDaysRemaining = remainingMonths - Math.Truncate(remainingMonths);
            var remainingDays = Math.Truncate(avgDaysRemaining * 30.436806);

            lblRemainingLife.Text = $"Estimated Remaining Life expectancy is {Math.Truncate(avgYearsRemaing)}" +
            $" year(s), {Math.Truncate(remainingMonths)}" +
            $" month(s) and {remainingDays}" +
            " day(s)";
        }

        /// <summary>
        /// On date ddl select event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnDateSelected(object sender, EventArgs e)
        {
            
        }
        /// <summary>
        /// Bind the countries list to the selected list
        /// </summary>
        private void populateCountryList()
        {
            var picker = new Picker { Title = "Select a country" };
            picker.SetBinding(Picker.ItemsSourceProperty, "Countries");
            picker.SetBinding(Picker.SelectedItemProperty, "SelectedCountry");
            picker.ItemDisplayBinding = new Binding("Name");            
        }
    }
}
