﻿using LifeExpectancy_.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LifeExpectancy_.ViewModels
{
    public class LifeExpectancyPageViewModel : ViewModelBase
    {
        public IList<Country> Countries {  get { return CountriesData.Countries; } }

        Country selectedCountry;

        public Country SelectedCountry
        {
            get { return selectedCountry; }
            set
            {
                if(selectedCountry != value)
                {
                    selectedCountry = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
