﻿using DeathCounter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LifeExpectancy_.Manager
{
    public class LifeExpectancyManager
    {
        /// <summary>
        /// Web Servie call to get average life expectancy by country
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public async Task<string> GetLifeExpectancyByCountryAsync(string countryCode)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://apps.who.int/gho/athena/api/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync($"GHO/WHOSIS_000001.json?filter=COUNTRY:{countryCode};YEAR:2016;SEX:MLE");
            try
            {
                if (response.IsSuccessStatusCode)
                {
                    var expectancyResponse = await response.Content.ReadAsAsync<RootObject>();
                    return expectancyResponse.fact.Select(x => x.value.display).SingleOrDefault();
                }
                else
                    return string.Empty;
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
